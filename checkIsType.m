function f = checkIsType(x, type)
  
  if ~isa(x, type)
    error('Error. \nInput must be a %s, not a %s.', type, class(x));
  endif
  
endfunction