function [x y z] = processPixel(image, offsets, pixelRow, pixelColumn, criteria, forwardTransform, inverseTransform)
  
  candidates = [];
  for i = 1:columns(offsets)
    rowOffset = offsets(1, i);
    columnOffset = offsets(2, i);
    
    neighborRow = pixelRow + rowOffset;
    neighborColumn = pixelColumn + columnOffset;
    
    if areValidCoordinates(image, neighborRow, neighborColumn)
      x = image(neighborRow, neighborColumn, 1);
      y = image(neighborRow, neighborColumn, 2);
      z = image(neighborRow, neighborColumn, 3);
      
      encodedNeighbor = forwardTransform(x, y, z);
      checkIsType(encodedNeighbor, 'uint64');
      candidates = [candidates encodedNeighbor]; 
    endif
        
  endfor
  
  encodedNeighbor = criteria(candidates); # TODO Param for morph operation
  
  [x y z] = inverseTransform( encodedNeighbor );
  checkIsPixel(x, y, z);
  
endfunction
