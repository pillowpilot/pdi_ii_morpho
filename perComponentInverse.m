# Based on RGB to HSI transformation and sorting by I-component
# I = (R + G + B) / 3;

function [r g b] = perComponentInverse(combined)
  
  checkIsType(combined, 'uint64');
  
  b = mod(combined, 2^8);
  combined = bitshift(combined, -8);
  g = mod(combined, 2^8);
  combined = bitshift(combined, -8);
  r = mod(combined, 2^8);
  
  r = uint8(r);
  g = uint8(g);
  b = uint8(b);
  
endfunction
