function flag = areValidCoordinates(image, row, column)
  
  flag = 1 <= row && row <= rows(image) && 1 <= column && column <= columns(image);
  
endfunction
