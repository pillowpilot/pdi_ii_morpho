function outputImage = applier(image, kernel, criteria, forwardTransform, inverseTransform)
  
  outputImage = zeros(size(image), 'uint8');
  
  offsets = kernelToOffsetList(kernel);
  for row = 1:rows(image)
    printf('Processing row %d of %d.\n', row, rows(image));
    
    for column = 1:columns(image)
      
      [x y z] = processPixel(image, offsets, row, column, criteria, forwardTransform, inverseTransform);
      outputImage(row, column, 1) = x;
      outputImage(row, column, 2) = y;
      outputImage(row, column, 3) = z;
      
    endfor
  endfor
  
endfunction
