function [r g b] = lexicographicalInverse(encoded)
  
  checkIsType(encoded, 'uint64');
  
  b = mod(encoded, 2^8);
  encoded = bitshift(encoded, -8);
  g = mod(encoded, 2^8);
  encoded = bitshift(encoded, -8);
  r = mod(encoded, 2^8);
  
  r = uint8(r);
  g = uint8(g);
  b = uint8(b);
  
endfunction
