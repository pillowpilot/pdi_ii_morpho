function combined = lexicographicalTransformer(x, y, z)
  
  checkIsPixel(x, y, z);
  
  #binaryString = [ dec2bin(x, 8) dec2bin(y, 8) dec2bin(z, 8)  ];
  #decimalCombined = bin2dec(binaryString);
  #combined = uint64(decimalCombined);
  
  x = uint64(x);
  y = uint64(y);
  z = uint64(z);
  
  combined = x*(2^16) + y*(2^8) + z;
  combined = uint64(combined);
  
endfunction
