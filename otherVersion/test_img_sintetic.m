clear

I=imread('img_sintetic.tiff');
%I=imread('rgb.tif');

n=10;
SE=ones(n,n);

I(:,:,1)=imdilate(I(:,:,1), SE); %% dilatamos el rojo
I(:,:,2)=imdilate(I(:,:,2), SE); %% dilatamos el verde
I(:,:,3)=imdilate(I(:,:,3), SE); %% dilatamos el azul

imshow(I);

