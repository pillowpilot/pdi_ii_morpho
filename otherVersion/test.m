clc;    % Clear the command window.
clear all;
close all;
%workspace;  % Make sure the workspace panel is showing.
format long g;
format compact;
fontsize = 24;
% Read in original image.
grayImage = imread('img.tif');
% grayImage = imread('moon.tif');
[rows, columns, numberOfColorChannels] = size(grayImage);
subplot(2,2,1);
imshow(grayImage);
axis on;
title('Original image', 'Fontsize', fontsize);
% Set up figure properties:
% Enlarge figure to full screen.
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
% Get rid of tool bar and pulldown menus that are along top of figure.
set(gcf, 'Toolbar', 'none', 'Menu', 'none');
% Give a name to the title bar.
set(gcf, 'Name', 'Demo by ImageAnalyst', 'NumberTitle', 'Off') 
drawnow;
% The user wanted a binary image for some reason, though it was never used.
% So calculate it anyway though it's not used or needed for the local min image.
binaryImage=im2bw(grayImage);
subplot(2,2,2);
imshow(binaryImage);
axis on;
title('Binary image that is not used or needed', 'Fontsize', fontsize);
% Define structuring element.
se = logical([0 1 0 ; 1 1 1 ; 0 1 0]);
[p, q]=size(se);
halfHeight = floor(p/2);
halfWidth = floor(q/2);
% Initialize output image
localMinImage = zeros(size(grayImage), class(grayImage));
% Perform local min operation, which is morphological erosion.
for col = (halfWidth + 1) : (columns - halfWidth)
  for row = (halfHeight + 1) : (rows - halfHeight)
    % Get the 3x3 neighborhood
    row1 = row-halfHeight;
    row2 = row+halfHeight;
    col1 = col-halfWidth;
    col2 = col+halfWidth;
    thisNeighborhood = grayImage(row1:row2, col1:col2);
    % Apply the structuring element
    pixelsInSE = thisNeighborhood(se);
    localMinImage(row, col) = min(pixelsInSE);
  end
end
subplot(2,2,3);
imshow(localMinImage);
axis on;
title('Eroded image', 'Fontsize', fontsize);