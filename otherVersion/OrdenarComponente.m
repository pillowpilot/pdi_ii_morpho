function [ vect ] = OrdenarComponente( subMat, numComponente)
%% ref. pag. 83 Tesis Doctoral, Ortiz Zamora, Francisco
% La ordenaci�n vectorial mediante una componente es, quiz�s, la aproximaci�n vectorial m�s
% sencilla. Consiste en ordenar vectores atendiendo al valor de una �nica componente,
% previamente definida como fuente del orden. El orden en este caso se reduce a una comparaci�n
% escalar. Se deduce que este tipo de relaci�n no es antisim�trica, por lo que se definir�
% formalmente como un preorden o tal que:
% para todo (p = (x, y, z), q = (x', y', z'))que pertenece a Z3, p <= q si solo si  x <= x'






end

