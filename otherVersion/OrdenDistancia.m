function [ vect ] = OrdenDistancia( subMatrix, coordRef)
%% ref. pag. 91 Tesis Doctoral, Ortiz Zamora, Francisco
% Este m�todo de orden se basa en el c�lculo de una distancia a un p�xel de referencia,
% previamente definido. En la mayor�a de los casos el p�xel de referencia es la coordenada de
% color negro, (0,0,0) en RGB. [Comer, 1998] emplea la norma eucl�dea como m�todo de
% ordenaci�n de pixels sobre la base RGB, de esta manera:
%(p(R)^2 + p(G)^2 + p(B)^2)^(0.5) <= (q(R)^2 + q(G)^2 + q(B)^2)^(0.5)
% definici�n de un elemento estructurante de tama�o 3x3.
len=size(subMatrix);
m=len(1);
n=len(2);
vect=zeros(m,n); % creamos una matriz de tamanho del submatrix
R=1;
G=2;
B=3;
% nos ponemos a calcular la distancia de todos los pixceles en referencia a coordRef

for i=1:m
	for j=1:n
		% obtenemos los elementos RGB
		p=subMatrix(i,j,:);
		
		% aqui calculamos la distancia
		d=(p(R)^2 + p(G)^2 + p(B)^2)^(0.5);
		vect(i,j)=d;
	end
end

end

