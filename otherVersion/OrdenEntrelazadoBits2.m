function [ vect ] = OrdenEntrelazadoBits( I )
% obtiene una sub matriz y convierte en un vector pero con los bits combinados

%% aqui obtenemos las dimensiones de submat
% convertimos cada pixcel que es un vector de 3 elementos, en un entero y agregamos
% en un vector que tiene la dimensi�n de tantos elementos como p�xceles se encuentren
% en submat
dim=size(I);
m=dim(1);
n=dim(2);

vect = zeros(m,n,3);
% el recorrido que ahcemos es primero por pixceles
for i=1:m
	for j=1:n
		%% convertimos el valor entero a binario de 24 bits 
		b=dec2bin(I(i,j),24)';

		e=reshape(b, [3,8]);
		
		%% aqui ya estamos situados en el pixcel I(m,n,:) que contiene los 3 elementos
		vect(i,j,1)=bin2dec(e(1));
		vect(i,j,2)=bin2dec(e(2));
		vect(i,j,3)=bin2dec(e(3));

		end
end


end

