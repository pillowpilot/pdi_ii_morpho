function [ vect ] = OrdenEntrelazadoBits( I )
% obtiene una sub matriz y convierte en un vector pero con los bits combinados

%% aqui obtenemos las dimensiones de submat
% convertimos cada pixcel que es un vector de 3 elementos, en un entero y agregamos
% en un vector que tiene la dimensi�n de tantos elementos como p�xceles se encuentren
% en submat
dim=size(I);
m=dim(1);
n=dim(2);
o=dim(3);
vect = zeros(m,n);
% el recorrido que ahcemos es primero por pixceles
for i=1:m
	for j=1:n
		%% aqui ya estamos situados en el pixcel I(m,n,:) que contiene los 3 elementos
		e1=I(m,n,1);
		e2=I(m,n,2);
		e3=I(m,n,3);
		
		%% al tener los valores, convertimos a binario
		b1=dec2bin(e1,8)';
		b2=dec2bin(e2,8)';
		b3=dec2bin(e3,8)';
		
		%% al tener los binarios, asignamos el nuevo valor transformando los bits
		num=[b1 b2 b3]';
		num=num(:)';		 % aqui tenemos el nuevo numero combinado con bits :'D
		
		vect(i,j)=bin2dec(num);
	end
end


end

