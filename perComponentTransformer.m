# Based on RGB to HSI transformation and sorting by I-component
# I = (R + G + B) / 3;

function combined = perComponentTransformer(r, g, b)
  
  checkIsPixel(r, g, b);
  
  r = uint64(r);
  g = uint64(g);
  b = uint64(b);
  
  sumation = r + g + b;
  
  combined = sumation * 4;                      # 2 bit left shift
  combined = combined + mod(sumation, 3);
  
  # Last 24 bits to store original RGB data
  combined = combined * (2^24);                 # 24 bit left shift
  combined = combined + r*(2^16) + g*(2^8) + b;
  
endfunction
