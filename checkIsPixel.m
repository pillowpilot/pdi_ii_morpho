function checkIsPixel(x, y, z)
  
  # Check if x, y and z are uint8
  type = 'uint8';
  checkIsType(x, type);
  checkIsType(y, type);
  checkIsType(z, type);
  
endfunction