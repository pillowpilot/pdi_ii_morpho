# Usage: 
# outD = erosion(in, kernel, @lexicographicalTransformer, @lexicographicalInverse);

function outputImage = erosion(inputImage, kernel, forwardTransform, inverseTransform)
  
  outputImage = applier(inputImage, kernel, @min, forwardTransform, inverseTransform);
  
endfunction
