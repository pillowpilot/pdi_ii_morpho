# Reporter

inImagePath = 'candy_icon.jpg';
outImagePath = 'firstOut.png';

in = imread(inImagePath);
blank = zeros(size(in), 'uint8');
kernel = [1 1 1; 1 1 1; 1 1 1];

outEL = erosion(in, kernel, @lexicographicalTransformer, @lexicographicalInverse);
outDL = dilation(in, kernel, @lexicographicalTransformer, @lexicographicalInverse);

outEpC = erosion(in, kernel, @perComponentTransformer, @perComponentInverse);
outDpC = dilation(in, kernel, @perComponentTransformer, @perComponentInverse);

report = [in outEL outDL; ...
          blank outEpC outDpC; ...
          blank abs(outEL - outEpC) abs(outDL - outDpC)];

imwrite(report, outImagePath);