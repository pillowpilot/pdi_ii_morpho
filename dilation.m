# Usage: 
# outD = dilation(in, kernel, @lexicographicalTransformer, @lexicographicalInverse);

function outputImage = dilation(inputImage, kernel, forwardTransform, inverseTransform)
  
  outputImage = applier(inputImage, kernel, @max, forwardTransform, inverseTransform);
  
endfunction
